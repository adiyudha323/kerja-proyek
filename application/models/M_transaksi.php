<?php 

class M_transaksi extends CI_Model{

	function gettransaksi($menu){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_selera','tb_selera.id_selera = tb_menu.id_selera');
		$this->db->where_in('id_menu', $menu);
		return $this->db->get()->result_array();
	}

	function gettransaksi1($menu){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_selera','tb_selera.id_selera = tb_menu.id_selera');
		$this->db->where_in('id_menu', $menu);
		$this->db->order_by('id_menu', 'asc');
		return $this->db->get()->result();
	}

	function transaksi(){
		$this->db->select('id_transaksi,no_transaksi,meja,pembeli,time,status,total,date_time');
		$this->db->from('tb_transaksi');
		$this->db->having('status = 1 OR status = 4');
		$this->db->order_by('id_transaksi asc');
		return $this->db->get()->result();
	}

	function transaksi2(){
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->where('status = 2');
		$this->db->order_by('id_transaksi asc');
		return $this->db->get()->result();
	}

	function rasa($rasa){
		$rasa = array_reverse($rasa);
		$result = array();
		foreach ($rasa as $val){
			$this->db->select('tb_rasa.*');
			$this->db->from('tb_menu');
			$this->db->join('tb_rasa','tb_rasa.id_selera = tb_menu.id_selera');
			$this->db->where('id_menu', $val);
			$result[] = $this->db->get()->result_array();
		}
		return $result;
		// $this->db->select('tb_rasa.*');
		// $this->db->from('tb_rasa');
		// $this->db->join('tb_menu','tb_menu.id_selera = tb_rasa.id_selera');
		// $this->db->where_in('id_menu', $rasa);
		// $this->db->order_by('tb_menu.id_menu', 'asc');
		// return $this->db->get()->result_array();
	}

	function insert($data){
		$query = $this->db->insert('tb_transaksi', $data);
		return ($this->db->affected_rows() > 0);
	}

	function create1($data){
		return	$this->db->insert('tb_transaksi', $data);
	}

	function create2($data2){
		return	$this->db->insert('tb_transaksi_master', $data2);
	}

	function update_stok($abcde,$id_menu){
		$query = $this->db->where('id_menu', $id_menu)->update('tb_menu', $abcde);
		return ($this->db->affected_rows());
	}

	function getorderadmin($no){
		
		$this->db->select('*');
		$this->db->from('tb_transaksi_master');
		// $this->db->join('tb_transaksi','tb_transaksi.id_transaksi = tb_transaksi_master.id_transaksi');
		$this->db->where('tb_transaksi_master.id_transaksi', $no);
		return $this->db->get()->result();
	}

	function gettotal($no){
		
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->where('id_transaksi', $no);
		return $this->db->get()->result();
	}

	function getdetailtotal($no){
		
		$this->db->select('*');
		$this->db->from('tb_transaksi_detail');
		$this->db->where('id_transaksi', $no);
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	function gettotal2($no){
		
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->where('id_transaksi', $no);
		return $this->db->get()->result_array();
	}
	
	function updateStatus($cek){
		$a = 2;
		$abcde = array(
			'status' => $a
		);
		$query = $this->db->where('id_transaksi', $cek)->update('tb_transaksi', $abcde);
		return ($this->db->affected_rows());
	}

	function updateStatus2($cek){
		$a = 3;
		$abcde = array(
			'status' => $a
		);
		$query = $this->db->where('id_transaksi', $cek)->update('tb_transaksi', $abcde);
		return ($this->db->affected_rows());
	}

	function save($data){
		$this->db->insert('tb_transaksi_detail', $data);
		return ($this->db->affected_rows());
	}

	function getidtransaksi($id){
		$this->db->select('id_transaksi');
		$this->db->where('no_transaksi',$id);
		$this->db->limit(1);
		$this->db->order_by('no_transaksi asc');
		$query = $this->db->get('tb_transaksi');
		return $query;
	}

	function detaillaporanuser($ses){
		$this->db->select('*');
		$this->db->from('tb_transaksi');
		$this->db->join('tb_transaksi_master','tb_transaksi.id_transaksi = tb_transaksi_master.id_transaksi ');
		$this->db->where('no_transaksi',$ses);
		return $this->db->get()->result(); 
	}

	function detailusertotal($no){
		
		$this->db->select('sum(total) as total');
		$this->db->from('tb_transaksi');
		$this->db->where('no_transaksi', $no);
		$this->db->limit(1);
		return $this->db->get()->result();
	}
	

	///
	function getnomor($a){
		$this->db->select('no_transaksi');
		$this->db->from('tb_transaksi');
		$this->db->where('id_transaksi', $a);
		$this->db->limit(1);
		return $this->db->get()->result();
	}

	function endid($a){
		$this->db->select('id_transaksi');
		$this->db->from('tb_transaksi');
		$this->db->where('no_transaksi', $a);
		$this->db->order_by('id_transaksi DESC');
		$this->db->limit(1);
		return $this->db->get();
	}
}

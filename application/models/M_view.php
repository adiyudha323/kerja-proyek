<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_view extends CI_Model {
  
	public function umum(){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori','tb_menu.id_kategori=tb_kategori.id_kategori');
		$this->db->join('tb_selera','tb_menu.id_selera=tb_selera.id_selera');
		// $this->db->join('tb_rasa','tb_menu.id_rasa=tb_rasa.id_rasa');
		// $this->db->join('tb_jnsmakanan','tb_menu.id_jenis=tb_jnsmakanan.id_jenis');
		$this->db->having('tb_menu.id_favorit = 4 AND tb_menu.id_kategori = 1');
		return $this->db->get()->result();
	}

	public function favorit(){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori','tb_menu.id_kategori=tb_kategori.id_kategori');
		$this->db->join('tb_selera','tb_menu.id_selera=tb_selera.id_selera');
		// $this->db->join('tb_rasa','tb_menu.id_rasa = tb_rasa.id_rasa');
		$this->db->join('tb_favorit','tb_menu.id_favorit=tb_favorit.id_favorit');
		// $this->db->join('tb_jnsmakanan','tb_menu.id_jenis=tb_jnsmakanan.id_jenis');
		$this->db->where('tb_menu.id_favorit = 1');
		return $this->db->get()->result();
	}

	public function minuman(){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori','tb_menu.id_kategori=tb_kategori.id_kategori');
		$this->db->join('tb_selera','tb_menu.id_selera=tb_selera.id_selera');
		$this->db->having('tb_menu.id_kategori = 2');
		return $this->db->get()->result();
	}

	public function snack(){
		$this->db->select('*');
		$this->db->from('tb_menu');
		$this->db->join('tb_kategori','tb_menu.id_kategori=tb_kategori.id_kategori');
		$this->db->join('tb_selera','tb_menu.id_selera=tb_selera.id_selera');
		$this->db->having('tb_menu.id_kategori = 3');
		return $this->db->get()->result();
	}
}

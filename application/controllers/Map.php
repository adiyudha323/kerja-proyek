
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	public function index(){
		$this->load->library('googlemaps');
		
		$config['center'] = '-8.087467,112.296663';//Coordinate tengah peta
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);
		
		$marker = array();
		$marker['position'] = '-8.087467,112.296663';//Posisi marker (itu tuh yang merah2 lancip itu loh :-p)
		$this->googlemaps->add_marker($marker);
		
		$data['map'] = $this->googlemaps->create_map();
		
		$this->load->view('dashboard/html/html_open');
		$this->load->view('dashboard/html/header');
		$this->load->view('dashboard/html/aside');
		$this->load->view('dashboard/tentang_kami',$data);
		$this->load->view('dashboard/html/footer');
		$this->load->view('dashboard/html/html_close');
	}
}

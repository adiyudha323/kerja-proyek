<?php 
class Pesan extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->model('m_pencarian');
		$this->load->model('m_view');
		$this->load->library('cart');
        // verify_session();
        $this->load->library('pagination');
		if (!isset($this->session->userdata['id_meja'])) {
			redirect(base_url("Login_user"));
		}
	}

	function index(){
		$this->load->database();
		$data['fav'] = $this->m_view->favorit();
		$data['umum'] = $this->m_view->umum();
		if (isset($_SESSION['cart'])) {
			setcookie("cartCookie", json_encode($_SESSION['cart']), time() + 3600, "/");
		  }else{
			setcookie("cartCookie", "", time() - 3600 );
		  }
		$this->load->view('pesan/html/html_open');
		$this->load->view('pesan/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/makanan',$data);
		$this->load->view('pesan/html/footer');
	}

	function minuman(){
		$this->load->database();;
		$data['minuman']=$this->m_view->minuman();
		if (isset($_SESSION['cart'])) {
			setcookie("cartCookie", json_encode($_SESSION['cart']), time() + 3600, "/");
		  }else{
			setcookie("cartCookie", "", time() - 3600 );
		  }
		$this->load->view('pesan/html/html_open');
		$this->load->view('pesan/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/minuman',$data);
		$this->load->view('pesan/html/footer');
	}

	function snack(){
		$this->load->database();;
		$data['Camilan']=$this->m_view->snack();
		if (isset($_SESSION['cart'])) {
			setcookie("cartCookie", json_encode($_SESSION['cart']), time() + 3600, "/");
		  }else{
			setcookie("cartCookie", "", time() - 3600 );
		  }
		$this->load->view('pesan/html/html_open');
		$this->load->view('pesan/html/header');
		$this->load->view('pesan/html/aside');
		$this->load->view('pesan/snack',$data);
		$this->load->view('pesan/html/footer');
	}

	public function logout_user()
	{
		$this->session->sess_destroy();
		$this->cart->destroy();
		delete_cookie('cartCookie');
		redirect('login_user');
	}

	
}

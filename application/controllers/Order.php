<?php 
class Order extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->model('m_transaksi');
		$this->load->helper('url');
		$this->load->library('fungsi');
		if (!isset($this->session->userdata['id_admin'])) {
			redirect(base_url("Login"));
		}
	}

	function index(){
		$this->load->view('admin/order/index');
	}

	function fetchTransaksi(){
		$result['data'] = array();
		foreach ($this->m_transaksi->transaksi() as $key => $value) {
			if ($value->status == 1){
				$status = "<center><span class='label label-success'>Baru</span></center>";
			} else {
				$status = "<center><span class='label label-info'>Tambah</span></center>";
			}
			$button = "<a href='order/proses/".$value->id_transaksi."'><center><button type='button' class='btn btn-default' title='Cetak'><i class='fa fa-credit-card'></i></button></center></a>";
			
			$result['data'][$key] = array($value->no_transaksi, $value->meja, $value->pembeli, $value->time, $status, $button);
		}
		echo json_encode($result);
	}

	function proses($no){
		// print_r($no);
		// $id = $this->m_transaksi->getnomor($no);
		// $endid = $this->m_transaksi->endid($id);
		$result['data']= $this->m_transaksi->getorderadmin($no);
		$result['total']= $this->m_transaksi->gettotal($no);
		// print_r($endid);
		if(empty($no)){
			redirect('order');
		} else {
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/order/proses_cetak',$result);
		$this->load->view('admin/html/footer');
		}
	}

	function cetak($cek){
	// print_r($cek);
		$result['data']= $this->m_transaksi->getorderadmin($cek);
		$result['total']= $this->m_transaksi->gettotal($cek);
		// $this->load->view('admin/order/cetak',$result);
		$ceks= $this->m_transaksi->updateStatus($cek);
		if($ceks == 1){
			$this->load->view('admin/order/cetak',$result);
		} else {
			redirect('order');
		}
	}

	function orderProses(){
		$this->load->view('admin/order/order');
	}

	function fetchTransaksi2(){
		$result['data'] = array();
		foreach ($this->m_transaksi->transaksi2() as $key => $value) {
			if ($value->status == 2){
				$status = "<center><span class='label label-success'>Proses</span></center>";
			} else {
				$status = "<span class='label label-danger'>Proses</span>";
			}
			$button = "<a href='proses_bayar/".$value->id_transaksi."'><center><button type='button' class='btn btn-default' title='Bayar'><i class='fa fa-credit-card'></i></button></center></a>";
			
			$result['data'][$key] = array($value->no_transaksi, $value->meja, $value->pembeli, $value->time, $status, $button);
		}
		echo json_encode($result);
	}

	function proses_bayar($no){
		// print_r($no);
		$result['data']= $this->m_transaksi->getorderadmin($no);
		$result['total']= $this->m_transaksi->gettotal2($no);
		// print_r($result);
		if(empty($no)){
			redirect('order');
		} else {
		$this->load->view('admin/html/html_open');
		$this->load->view('admin/html/header');
		$this->load->view('admin/html/aside');
		$this->load->view('admin/order/order_proses',$result);
		$this->load->view('admin/html/footer');
		}
	}

	function bayardetail($a){
		// print_r($a);
		$getid = $this->m_transaksi->getidtransaksi($a)->result_array();
		// print_r($b);
		$data = array(
			'id_transaksi' => $getid['0']['id_transaksi'],
			'bayar' => $this->input->post('bayar'),
			'diskon' => $this->input->post('diskon'),
			'kembalian' => $this->input->post('kembalian')
		);
		if($this->m_transaksi->save($data) == true){
			$this->m_transaksi->updatestatus2($a);
			redirect('order/cetak_bayar/'.$a);
		} else {
			redirect('order/bayardetail');
		}
	}

	function cetak_bayar($cek){
		// $getid = $this->m_transaksi->getidtransaksi($cek)->result_array();
		// $id = $getid['0']['id_transaksi'];
		$result['data']= $this->m_transaksi->getorderadmin($cek);
		$result['total']= $this->m_transaksi->gettotal($cek);
		$result['detail']= $this->m_transaksi->getdetailtotal($cek);
		$this->load->view('admin/order/cetak_bayar',$result);
	}
}

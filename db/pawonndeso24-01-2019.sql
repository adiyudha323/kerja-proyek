-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Jan 2019 pada 03.27
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pawonndeso`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(10) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `username` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `status` int(5) DEFAULT NULL,
  `level` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `name`, `username`, `email`, `password`, `telepon`, `gambar`, `status`, `level`) VALUES
(71, 'admin', 'pawonndeso', 'pawon@pawon', '5f4dcc3b5aa765d61d8327deb882cf99', '098765432120', 'wajah_laki1.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_favorit`
--

CREATE TABLE `tb_favorit` (
  `id_favorit` int(30) NOT NULL,
  `nama_favorit` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_favorit`
--

INSERT INTO `tb_favorit` (`id_favorit`, `nama_favorit`) VALUES
(1, 'makanan favorit'),
(2, 'minuman favorit'),
(3, 'snack favorit'),
(4, 'umum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jnsmakanan`
--

CREATE TABLE `tb_jnsmakanan` (
  `id_jenis` int(11) NOT NULL,
  `jenis_makanan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jnsmakanan`
--

INSERT INTO `tb_jnsmakanan` (`id_jenis`, `jenis_makanan`) VALUES
(1, 'Ayam'),
(2, 'Gurami'),
(3, 'Udang'),
(4, 'Daging'),
(5, 'Iga'),
(6, 'Nasi'),
(7, 'Sayur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(30) NOT NULL,
  `nama_kategori` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Makanan'),
(2, 'Minuman'),
(3, 'Snack');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_meja`
--

CREATE TABLE `tb_meja` (
  `id_meja` int(11) NOT NULL,
  `nama_meja` varchar(30) NOT NULL,
  `kapasitas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_meja`
--

INSERT INTO `tb_meja` (`id_meja`, `nama_meja`, `kapasitas`) VALUES
(1, 'GAKA 1', 8),
(2, 'GAKA 2', 8),
(3, 'GAKA 3', 4),
(4, 'GAKA 4', 4),
(18, 'GAKI 1A', 8),
(19, 'GAKI 1', 4),
(20, 'GAKI 2', 4),
(21, 'GAKI 3', 4),
(22, 'GAKI 4', 4),
(23, 'GAKI 5', 4),
(24, 'GAKI 6', 4),
(25, 'GAKI A', 4),
(26, 'GAKI B', 4),
(27, 'GAKI C', 8),
(28, 'GAKI D', 4),
(29, 'GABA 1', 4),
(30, 'GABA 2', 4),
(31, 'GABA 3', 4),
(32, 'GABA 4', 8),
(33, 'GABA 5', 4),
(34, 'GABA 6', 8),
(35, 'BT', 8),
(36, 'BTT', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id_menu` int(30) NOT NULL,
  `id_kategori` int(30) DEFAULT NULL,
  `id_favorit` int(30) DEFAULT NULL,
  `id_selera` int(11) DEFAULT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama_menu` varchar(30) DEFAULT NULL,
  `harga` int(10) DEFAULT NULL,
  `stok_menu` varchar(11) NOT NULL,
  `gambar` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`id_menu`, `id_kategori`, `id_favorit`, `id_selera`, `id_jenis`, `nama_menu`, `harga`, `stok_menu`, `gambar`) VALUES
(1, 1, 4, 15, 1, 'Ayam Geprek', 15000, '10', 'Ayam_Geprek.jpg'),
(2, 1, 4, 8, 1, 'Ayam Kremes', 13500, '10', 'Ayam_Kremes.jpg'),
(3, 1, 4, 12, 4, 'Beef Steak', 45000, '4', 'beef_steak.jpg'),
(4, 1, 4, 13, 7, 'Cah Kangkung', 15000, '7', 'cah_kangkung.jpg'),
(5, 1, 1, 1, 2, 'Gurame Bakar', 45000, '0', 'gurame_bakar.jpg'),
(6, 1, 1, 1, 5, 'Iga Bakar', 25000, '0', 'iga_bakar.jpeg'),
(7, 1, 4, 11, 6, 'Nasi Goreng', 15000, '0', 'nasi_goreng.jpg'),
(8, 1, 4, 10, 1, 'Sup Ceker', 15000, '1', 'sup_ceker.jpg'),
(9, 1, 4, 14, 3, 'Udang Asam', 15000, '1', 'udang_asam.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_rasa`
--

CREATE TABLE `tb_rasa` (
  `id_rasa` int(11) NOT NULL,
  `id_selera` int(11) NOT NULL,
  `rasa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_rasa`
--

INSERT INTO `tb_rasa` (`id_rasa`, `id_selera`, `rasa`) VALUES
(1, 1, 'Mercon'),
(2, 1, 'Madu'),
(3, 1, 'Asap'),
(4, 8, 'Kremes'),
(5, 8, 'Mercon'),
(6, 8, 'Tanpa Sambal'),
(7, 14, 'Asam manis'),
(8, 14, 'Asam pedas'),
(9, 1, 'Tampa bumbu'),
(10, 10, 'Original'),
(11, 10, 'Asam pedas'),
(12, 11, 'Mercon'),
(13, 11, 'Ayam sosis'),
(14, 11, 'Khas pawon ndeso'),
(15, 12, 'Original'),
(16, 12, 'Black paper'),
(17, 13, 'Original'),
(18, 13, 'Mercon'),
(19, 9, 'Mayonnaise'),
(20, 9, 'Goreng'),
(21, 15, 'Mercon'),
(22, 15, 'Original'),
(23, 15, 'Sambal pisah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_selera`
--

CREATE TABLE `tb_selera` (
  `id_selera` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `selera` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_selera`
--

INSERT INTO `tb_selera` (`id_selera`, `id_kategori`, `selera`) VALUES
(1, 1, 'Bakar'),
(8, 1, 'Goreng'),
(9, 1, 'Udang goreng'),
(10, 1, 'Sup'),
(11, 1, 'Nasi goreng'),
(12, 1, 'Panggang'),
(13, 1, 'Sayur'),
(14, 1, 'Asam'),
(15, 1, 'Penyet');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi`
--

CREATE TABLE `tb_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `no_transaksi` varchar(30) DEFAULT NULL,
  `meja` varchar(30) DEFAULT NULL,
  `pembeli` varchar(30) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date_time` date NOT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi`
--

INSERT INTO `tb_transaksi` (`id_transaksi`, `no_transaksi`, `meja`, `pembeli`, `total`, `status`, `date_time`, `time`) VALUES
(163, 'T192300001', 'GAKI 1A', 'Yudha', 75000, 1, '2019-01-23', '18:42:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_detail`
--

CREATE TABLE `tb_transaksi_detail` (
  `id_transaksi_detail` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi_detail`
--

INSERT INTO `tb_transaksi_detail` (`id_transaksi_detail`, `id_transaksi`, `bayar`, `diskon`, `kembalian`) VALUES
(58, 163, 100000, 10, 32500);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_transaksi_master`
--

CREATE TABLE `tb_transaksi_master` (
  `id_transaksi_master` int(11) NOT NULL,
  `id_transaksi` int(11) DEFAULT NULL,
  `menu` varchar(30) DEFAULT NULL,
  `selera` varchar(30) NOT NULL,
  `rasa` varchar(30) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_transaksi_master`
--

INSERT INTO `tb_transaksi_master` (`id_transaksi_master`, `id_transaksi`, `menu`, `selera`, `rasa`, `jumlah`, `harga`, `subtotal`) VALUES
(120, 158, 'Udang Asam', 'Asam', 'Asam manis', 1, 15000, 15000),
(121, 159, 'Udang Asam', 'Asam', 'Asam manis', 1, 15000, 15000),
(122, 160, 'Udang Asam', 'Asam', 'Asam manis', 1, 15000, 15000),
(123, 161, 'Sup Ceker', 'Sup', 'Original', 1, 15000, 15000),
(124, 160, 'Cah Kangkung', 'Sayur', 'Original', 1, 15000, 15000),
(125, 163, 'Beef Steak', 'Panggang', 'Original', 1, 45000, 45000),
(126, 163, 'Cah Kangkung', 'Sayur', 'Original', 2, 15000, 30000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `nama_user` varchar(20) DEFAULT NULL,
  `alamat_user` varchar(20) DEFAULT NULL,
  `no_telp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_favorit`
--
ALTER TABLE `tb_favorit`
  ADD PRIMARY KEY (`id_favorit`);

--
-- Indexes for table `tb_jnsmakanan`
--
ALTER TABLE `tb_jnsmakanan`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_meja`
--
ALTER TABLE `tb_meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_favorit` (`id_favorit`),
  ADD KEY `id_kategori_2` (`id_kategori`);

--
-- Indexes for table `tb_rasa`
--
ALTER TABLE `tb_rasa`
  ADD PRIMARY KEY (`id_rasa`);

--
-- Indexes for table `tb_selera`
--
ALTER TABLE `tb_selera`
  ADD PRIMARY KEY (`id_selera`);

--
-- Indexes for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tb_transaksi_detail`
--
ALTER TABLE `tb_transaksi_detail`
  ADD PRIMARY KEY (`id_transaksi_detail`);

--
-- Indexes for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  ADD PRIMARY KEY (`id_transaksi_master`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `tb_favorit`
--
ALTER TABLE `tb_favorit`
  MODIFY `id_favorit` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_jnsmakanan`
--
ALTER TABLE `tb_jnsmakanan`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_meja`
--
ALTER TABLE `tb_meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id_menu` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_rasa`
--
ALTER TABLE `tb_rasa`
  MODIFY `id_rasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_selera`
--
ALTER TABLE `tb_selera`
  MODIFY `id_selera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_transaksi`
--
ALTER TABLE `tb_transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `tb_transaksi_detail`
--
ALTER TABLE `tb_transaksi_detail`
  MODIFY `id_transaksi_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `tb_transaksi_master`
--
ALTER TABLE `tb_transaksi_master`
  MODIFY `id_transaksi_master` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD CONSTRAINT `tb_menu_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id_kategori`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_menu_ibfk_2` FOREIGN KEY (`id_favorit`) REFERENCES `tb_favorit` (`id_favorit`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
